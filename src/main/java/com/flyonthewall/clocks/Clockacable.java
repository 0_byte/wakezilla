package com.flyonthewall.clocks;

/**
 * @author obyte
 * Interface for describing some actions, when Clocks timer rings up
 */
public interface Clockacable {
    
    public void doAlarm();
    
    public void updateAlarmState();
    
}
