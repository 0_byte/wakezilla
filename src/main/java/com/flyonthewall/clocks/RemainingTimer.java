package com.flyonthewall.clocks;

/**
 * @author 0byte
 */
public class RemainingTimer {

    private int remH, remM,
            currH, currM,
            wakeH, wakeM;

    public RemainingTimer() {
    }

    public void setTime(int currH, int currM, int wakeH, int wakeM) {
        this.currM = currM;
        this.currH = currH;
        this.wakeM = wakeM;
        this.wakeH = wakeH;
    }

    public void calculateTime() {
        if (wakeH < currH) {
            remH = wakeH + 24 - currH;
        } else {
            remH = wakeH - currH;
        }

        remM = wakeM - currM;
        if (remM < 0) {
            remH--;
            remM = remM + 60;
        }
    }

    public int getRemH() {
        return remH;
    }

    public int getRemM() {
        return remM;
    }
    
    public String getFormatedRemainingTime() {
        String remHour = Integer.toString(getRemH());
        String remMin = Integer.toString(getRemM());
        
        String remTime = "";
        return remTime = addZero(remHour, remTime) 
                + " : " + addZero(remMin, remTime);
    }
    
    private String addZero(String str, String remStr){
        if (str.length() < 2) {
            return remStr += "0" +  str;
        }else{
            return remStr += str;
        }
    }
}
