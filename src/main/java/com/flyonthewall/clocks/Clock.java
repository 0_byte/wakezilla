package com.flyonthewall.clocks;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.Timer;

/**
 * @author obyte
 */
public class Clock {

    private int alarmHour = 7;
    private int alarmMin = 0;
    private int timerDelay = 3000;
    private Thread timerThread;
    private Timer timer;
    private GregorianCalendar gCalendar;
    private ClockTask cT;
    private RemainingTimer rmT;

    public Clock(Clockacable cl) {
        cT = new ClockTask(cl);
        rmT = new RemainingTimer();

    }

    public void startClock() {
        timerThread = new Thread(new Runnable() {
            public void run() {

                timer = new Timer(timerDelay, new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        gCalendar = new GregorianCalendar();
                        gCalendar.getTime();
                        int currMin = gCalendar.get(Calendar.MINUTE);
                        int currHour = gCalendar.get(Calendar.HOUR_OF_DAY);

                        rmT.setTime(currHour, currMin, alarmHour, alarmMin);
                        rmT.calculateTime();
                        
                        cT.updateAlarm();
                        
                        if (alarmHour == currHour && alarmMin == currMin) {
                            cT.doTask();
                            stopClock();
                        }
                    }
                });
                
                timer.setInitialDelay(0);
                timer.start();
            }
        });

        timerThread.start();

    }

    public void stopClock() {
        if (timer.isRunning()) {
            timer.stop();
        }

        if (timerThread.isAlive()) {
            timerThread = null;
        }
    }

    public int getAlarmHour() {
        return alarmHour;
    }

    public void setAlarmHour(int alarmHour) {
        this.alarmHour = alarmHour;
    }

    public int getAlarmMin() {
        return alarmMin;
    }

    public void setAlarmMin(int alarmMin) {
        this.alarmMin = alarmMin;
    }

    public void setNewClockTask(Clockacable cl) {
        cT = new ClockTask(cl);
    }

    public RemainingTimer getRmT() {
        return rmT;
    }

    public int getTimerDelay() {
        return timerDelay;
    }
}
