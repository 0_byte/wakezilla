package com.flyonthewall.clocks;

/**
 * @author obyte
 */
public class ClockTask {
    
    
    Clockacable currClockacable;
    
    /*you must create new class and imlement in him Clockacable interface
     * 
     */
    public ClockTask(Clockacable cl) {
        currClockacable = cl;
    }    
    
    public void doTask(){
        currClockacable.doAlarm();
    }
    
    public void updateAlarm(){
        currClockacable.updateAlarmState();
    }
    
}
