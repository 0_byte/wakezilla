package com.flyonthewall.wakezilla;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.JavaSoundAudioDevice;
import javazoom.jl.player.advanced.AdvancedPlayer;

/**
 * @author obyte
 */
public class Player {

    private AdvancedPlayer ap;
    private File songFile;

    public Player(File songFile) {
        this.songFile = songFile;
    }

    public void playFile() {

        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    InputStream is = new FileInputStream(songFile);
                    JavaSoundAudioDevice auDev = new JavaSoundAudioDevice();

                    ap = new AdvancedPlayer(is, auDev);
                    ap.play();

                } catch (FileNotFoundException ex) {
                    System.out.println("File not found");
                } catch (JavaLayerException jle) {
                    System.out.println("Jle exeption");
                }
            }
        };
        Thread th = new Thread(runnable);
        th.start();
    }

    public void stopPlaying() {
        if (ap != null) {
            ap.close();
        }
    }
}
