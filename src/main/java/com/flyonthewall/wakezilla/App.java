package com.flyonthewall.wakezilla;

import javax.swing.UIManager;

public class App {

    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                WakeGui wakeGui = new WakeGui();
                wakeGui.setVisible(true);
                wakeGui.setLocation(300, 200);
            }
        });    
        
    }
}
